import React from "react";
import { StyleSheet, Text, View, StatusBar } from "react-native";

import colors from "../config/colors";

export default function ScreenContainer({ children, title }) {
  return (
    <View style={{ flex: 1 }}>
      <StatusBar backgroundColor={colors.primaryDark} />
      <View style={styles.appBar}>
        <Text style={styles.title}>{title}</Text>
      </View>
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  appBar: {
    width: "100%",
    justifyContent: "center",
    backgroundColor: colors.primary,
    color: colors.primary,
    height: 56,
    paddingStart: 16,
    elevation: 4,
  },
  title: {
    color: colors.secondary,
    fontSize: 24,
  },
});
