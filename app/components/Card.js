import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";

import colors from "../config/colors";
import strings from "../config/strings";

const deviceWidth = Dimensions.get("window").width;

export default function Card({ data, onPress }) {
  return (
    <TouchableOpacity
      onPress={() => {
        onPress(data);
      }}
    >
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: data.posterImage.small }} />
        <View style={styles.containerInfo}>
          <View style={styles.containerTitle}>
            <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
              {data.titles.en_jp}
            </Text>
            <Text style={styles.episode} numberOfLines={1} ellipsizeMode="tail">
              {strings.labelEpisode}
              {data.episodeCount}
            </Text>
          </View>
          <View style={styles.containerRate}>
            <Icon name="star-rate" size={24} color={colors.white} />
            <Text style={styles.rate}>{data.averageRating}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 8,
    borderRadius: 10,
    backgroundColor: colors.accent,
    height: 200,
    elevation: 4,
    justifyContent: "flex-end",
    width: deviceWidth * 0.435,
  },
  containerInfo: {
    backgroundColor: colors.primary,
    borderRadius: 10,
    flexDirection: "row",
    padding: 8,
    justifyContent: "space-between",
  },
  containerTitle: {
    paddingEnd: 8,
    justifyContent: "center",
    width: deviceWidth * 0.25,
  },
  containerRate: {
    padding: 4,
    backgroundColor: colors.accent,
    borderRadius: 10,
    elevation: 4,
    width: 40,
    alignItems: "center",
  },
  episode: {
    color: colors.secondary,
    fontSize: 12,
  },
  image: {
    width: "100%",
    height: "100%",
    position: "absolute",
    borderRadius: 10,
  },
  title: {
    color: colors.secondary,
    fontSize: 16,
    fontWeight: "bold",
  },
  rate: {
    marginTop: 4,
    color: colors.white,
    fontSize: 12,
  },
});
