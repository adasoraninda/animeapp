export default {
  primary: "#D0DBDE",
  primaryDark: "#BDCFD4",
  accent: "#60CAE5",
  background: "#F6F6F6",
  secondary: "#003131",
  white: "#FFFFFF",
};
