export default {
  appName: "Anime App",
  labelEpisode: "Episode: ",
  labelDescription: "Description: ",
  git: "Git:",
  titleList: "List",
  titleAbout: "About",
  name: "Adadua karunia putera",
  gitAccount: "@adasoraninda",
  job: "Android Developer",
  profileUri:
    "https://d17ivq9b7rppb3.cloudfront.net/small/avatar/202002241711284efa7d97fb56b052bf1f0b7f1d5b03af.png",
};
