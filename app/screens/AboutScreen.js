import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";

import ScreenContainer from "../components/ScreenContainer";

import colors from "../config/colors";
import strings from "../config/strings";

export default function AboutScreen() {
  return (
    <ScreenContainer title={strings.titleAbout}>
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: strings.profileUri }} />
        <Text style={styles.nameBio}>{strings.name}</Text>
        <Text style={styles.jobBio}>{strings.job}</Text>
        <View style={styles.lineContainer}>
          <View style={styles.line} />
          <Text style={styles.gitText}>{strings.git}</Text>
          <View style={styles.line} />
        </View>
        <View style={styles.gitContainer}>
          <GitBio
            icon={require("../assets/github-brands.png")}
            name={strings.gitAccount}
          />
          <GitBio
            icon={require("../assets/gitlab-brands.png")}
            name={strings.gitAccount}
          />
        </View>
      </View>
    </ScreenContainer>
  );
}

const GitBio = ({ icon, name }) => {
  return (
    <View style={styles.gitBioContainer}>
      <Image style={styles.gitImage} source={icon} />
      <Text style={styles.gitAccount}>{name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    padding: 16,
    justifyContent: "center",
    alignItems: "center",
  },
  gitContainer: {
    justifyContent: "space-evenly",
    flexDirection: "row",
  },
  gitText: {
    padding: 8,
    fontWeight: "bold",
  },
  gitBioContainer: {
    marginTop: 8,
    alignItems: "center",
    margin: 16,
  },
  gitAccount: {
    fontSize: 16,
    paddingTop: 8,
  },
  gitImage: {
    width: 50,
    height: 50,
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 120 / 2,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: colors.accent,
  },
  jobBio: {
    marginTop: 4,
    fontSize: 16,
  },
  line: {
    width: 100,
    height: 1,
    backgroundColor: colors.accent,
  },
  lineContainer: {
    marginTop: 8,
    flexDirection: "row",
    alignItems: "center",
  },
  nameBio: {
    marginTop: 24,
    fontSize: 18,
    fontWeight: "bold",
  },
});
