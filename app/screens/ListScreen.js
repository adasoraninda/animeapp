import React, { useState, useEffect } from "react";
import { FlatList, StyleSheet, View, ActivityIndicator } from "react-native";

import Card from "../components/Card";

import ScreenContainer from "../components/ScreenContainer";
import colors from "../config/colors";

import strings from "../config/strings";

const url = "https://kitsu.io/api/edge/anime";

export default function ListScreen({ navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((json) => setData(json.data))
      .catch((error) => alert(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <ScreenContainer title={strings.appName}>
      <View style={styles.container}>
        {isLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            data={data}
            keyExtractor={(item) => item.id}
            numColumns={2}
            renderItem={(data) => (
              <Card
                data={data.item.attributes}
                onPress={(data) =>
                  navigation.navigate("Detail", {
                    data: data,
                  })
                }
              />
            )}
          />
        )}
      </View>
    </ScreenContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 8,
    backgroundColor: colors.background,
    flex: 1,
  },
});
