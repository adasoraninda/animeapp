import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import Icon from "react-native-vector-icons/MaterialIcons";

import ListScreen from "./ListScreen";
import AboutScreen from "./AboutScreen";
import DetailScreen from "./DetailScreen";

import colors from "../config/colors";

const Tab = createBottomTabNavigator();
const RootStack = createStackNavigator();

const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="Home" component={TabScreen} />
    <RootStack.Screen name="Detail" component={DetailScreen} />
  </RootStack.Navigator>
);

const TabScreen = () => (
  <Tab.Navigator
    initialRouteName="List"
    screenOptions={({ route }) => ({
      tabBarIcon: ({ color }) => {
        let iconName;
        if (route.name == "List") {
          iconName = "list";
        } else if (route.name == "About") {
          iconName = "account-circle";
        }
        return <Icon name={iconName} size={36} color={color} />;
      },
    })}
    tabBarOptions={{
      showLabel: false,
      activeTintColor: colors.white,
      inactiveTintColor: colors.secondary,
      style: {
        backgroundColor: colors.primary,
        height: 60,
      },
    }}
  >
    <Tab.Screen name="List" component={ListScreen} />
    <Tab.Screen name="About" component={AboutScreen} />
  </Tab.Navigator>
);

export default function HomeScreen() {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
}
