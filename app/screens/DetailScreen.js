import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";

import colors from "../config/colors";
import strings from "../config/strings";

const deviceWidth = Dimensions.get("window").width;

export default function DetailScreen({ route, navigation }) {
  const data = route.params.data;
  return (
    <ScrollView>
      <View>
        <Image style={styles.image} source={{ uri: data.coverImage.large }} />
        <Icon
          style={styles.iconBack}
          name="arrow-back"
          size={36}
          color={colors.white}
          onPress={() => {
            navigation.goBack();
          }}
        />
        <View style={styles.containerContent}>
          <View style={styles.containerInfo}>
            <View style={styles.containerTitle}>
              <Text style={styles.title} numberOfLines={2} ellipsizeMode="tail">
                {data.titles.en_jp}
              </Text>
              <Text style={styles.episode}>
                {strings.labelEpisode}
                {data.episodeCount}
              </Text>
            </View>
            <View style={styles.containerRate}>
              <Icon name="star-rate" size={36} color={colors.accent} />
              <Text style={styles.rate}>{data.averageRating}</Text>
            </View>
          </View>
          <Text style={styles.labelDesc}>{strings.labelDescription}</Text>
          <Text>{data.description}</Text>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  containerContent: {
    paddingStart: 24,
    paddingEnd: 24,
    paddingTop: 16,
    paddingBottom: 16,
  },
  episode: {
    fontSize: 16,
  },
  iconBack: {
    position: "absolute",
    margin: 16,
  },
  image: {
    width: "100%",
    height: 200,
    resizeMode: "stretch",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  containerInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  containerTitle: {
    justifyContent: "center",
    width: deviceWidth * 0.7,
  },
  containerRate: {
    justifyContent: "flex-start",
  },
  labelDesc: {
    marginTop: 24,
    fontWeight: "bold",
  },
});
